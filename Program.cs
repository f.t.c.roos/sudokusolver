﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver
{
    class Program
    {
        // Specify the upper left coordinates of each block
        public static (int, int)[] blockCoords = { (0, 0), (3, 0), (6, 0), (0, 3), (3, 3), (6, 3), (0, 6), (3, 6), (6, 6) };

        // Contains list of the numbers that are fixed in each block [block, value]        
        public static bool [,] blockFixedNumbers = new bool [9, 9];

        // Contains the coordinates of the numbers that are fixed in each block
        public static bool [,] fixedCoords = new bool [9, 9];

        public static int[,] grid = new int[9, 9];

        public static int score = 0;
        public static int plateauThreshold = 100;
        public static int randomWalkLength = 10;
        public static Random random = new Random(); 


        static void Main(string[] args)
        {
            InitializeGrid();
            FillGrid();
            Evaluate();
            Print();
            
            IteratedLocalSearch();
            Print();

            Console.ReadKey();
        }
         
        public static void IteratedLocalSearch()
        {
            int plateauAmount = 0; // for how many iterations the score has stayed the same
            int previousScore = -1;

            while (score > 0)
            {
                while (plateauAmount < plateauThreshold)
                {
                    Iterate();
                    Console.WriteLine(score);
                    if (score == 0) // we found a solution
                    {
                        return;
                    }

                    plateauAmount = (score == previousScore) ? plateauAmount + 1 : 0;
                    previousScore = score;
                }

                // Execute random-walk
                for (int i = 0; i < randomWalkLength; i++) 
                {
                    IterateRandom();
                }

                plateauAmount = 0;
            }
        }

        // Perform one iteration while ignoring the score
        public static void IterateRandom()
        {
            int blockNumber = random.Next(9);

            int blockX = blockCoords[blockNumber].Item1;
            int blockY = blockCoords[blockNumber].Item2;

            int i, x, y;
            do
            {
                i = random.Next(9);
                x = blockX + (i % 3);
                y = blockY + (i / 3);
            }
            while (fixedCoords[x, y]);

            int j, x2, y2;
            do
            {
                j = random.Next(9);
                x2 = blockX + (j % 3);
                y2 = blockY + (j / 3);
            }
            while (fixedCoords[x2, y2] || (x == x2 && y == y2)); // can't be the same point

            x = blockX + (i % 3);
            y = blockY + (i / 3);

            x2 = blockX + (j % 3);
            y2 = blockY + (j / 3);

            if (x != x2)
            {
                score += ColumnScoreDiff(x, y, grid[x2, y2]) + ColumnScoreDiff(x2, y2, grid[x, y]);
            }

            if (y != y2)
            {
                score += RowScoreDiff(x, y, grid[x2, y2]) + RowScoreDiff(x2, y2, grid[x, y]);
            }

            int oldValue = grid[x, y];
            grid[x, y] = grid[x2, y2];
            grid[x2, y2] = oldValue;
        }

        // Perform one iteration of hill climbing with best/first-improvement
        public static void Iterate()
        {
            int blockNumber = random.Next(9);

            int newScore;
            int bestScore = score;
            int bestI = -1;
            int bestJ = -1;

            int blockX = blockCoords[blockNumber].Item1;
            int blockY = blockCoords[blockNumber].Item2;

            int x, y, x2, y2;

            for(int i = 0; i < 9; i++)
            {
                for(int j = i + 1; j < 9; j++)
                {
                    x = blockX + (i % 3);
                    y = blockY + (i / 3);

                    if (fixedCoords[x, y])
                    {
                        break;
                    }

                    x2 = blockX + (j % 3);
                    y2 = blockY + (j / 3);

                    if (fixedCoords[x2, y2])
                    {
                        continue;
                    }

                    newScore = score;

                    if (x != x2)
                    {
                        newScore += ColumnScoreDiff(x, y, grid[x2, y2]);
                        newScore += ColumnScoreDiff(x2, y2, grid[x, y]);
                    }

                    if (y != y2)
                    {
                        newScore += RowScoreDiff(x, y, grid[x2, y2]);
                        newScore += RowScoreDiff(x2, y2, grid[x, y]);
                    }

                    if (newScore <= bestScore)
                    {
                        bestScore = newScore;
                        bestI = i;
                        bestJ = j;
                    }

                }
            }

            // Execute swap
            if (bestI != -1)
            {
                x = blockX + (bestI % 3);
                y = blockY + (bestI / 3);

                x2 = blockX + (bestJ % 3);
                y2 = blockY + (bestJ / 3);

                int oldValue = grid[x, y];
                grid[x, y] = grid[x2, y2];
                grid[x2, y2] = oldValue;
            }

            // Update the score
            score = bestScore;
        }

        // Calculates the difference in row score when swapping two numbers
        public static int RowScoreDiff(int x, int y, int newValue)
        {
            int value = grid[x, y];

            bool newValuePresent = false;
            bool oldValuePresent = false;

            for(int i = 0; i < 9; i++)
            {
                if (grid[i, y] == newValue)
                {
                    newValuePresent = true;
                }
                if (grid[i, y] == value && i != x)
                {
                    oldValuePresent = true;
                }
            }

            int score = 0;
            if (!newValuePresent)
            {
                score--;
            }
            if (!oldValuePresent)
            {
                score++;
            }

            return score;
        }

        // Calculates the difference in column score when swapping two numbers
        public static int ColumnScoreDiff(int x, int y, int newValue)
        {
            int value = grid[x, y];

            bool newValuePresent = false;
            bool oldValuePresent = false;

            for (int i = 0; i < 9; i++)
            {
                if (grid[x, i] == newValue)
                {
                    newValuePresent = true;
                }
                if (grid[x, i] == value && i != y)
                {
                    oldValuePresent = true;
                }
            }

            int score = 0;
            if (!newValuePresent)
            {
                score--;
            }
            if (!oldValuePresent)
            {
                score++;
            }

            return score;
        }

        // Evaluate the score
        public static void Evaluate()
        {
            for (int i = 0; i < 9; i++)
            {
                score += RowScore(i);
                score += ColumnScore(i);
            }
        }

        // The amount of numbers that is missing in row y
        public static int RowScore(int y)
        {
            int score = 0;
            bool[] numbers = new bool[9];

            for (int x = 0; x < 9; x++)
            {
                numbers[grid[x, y]] = true;
            }

            for (int i = 0; i < 9; i++)
            {
                if (!numbers[i])
                {
                    score++;
                }
            }

            return score;
        }

        // The amount of numbers that is missing in column x
        public static int ColumnScore(int x)
        {
            int score = 0;
            bool[] numbers = new bool[9];

            for (int y = 0; y < 9; y++)
            {
                numbers[grid[x, y]] = true;
            }

            for (int i = 0; i < 9; i++)
            {
                if (!numbers[i])
                {
                    score++;
                }
            }

            return score;
        }
        

        // Prints the sudoku grid
        public static void Print()
        {
            for (int i = 0; i < 9; i++)
            {
                if(i == 3 || i == 6)
                {
                    for(int l = 0; l < 11; l++)
                    {
                        Console.Write('-');
                    }
                    Console.WriteLine();
                }
                
                for (int j = 0; j < 9; j++)
                {
                    if(j == 3 || j == 6)
                    {
                        Console.Write("|");
                    }
                    
                    Console.Write(grid[i, j] + 1);
                    
                }
                Console.WriteLine();
            }
        }

        // Parses the grid and keeps track of the fixed numbers
        public static void InitializeGrid()
        {
            // Grid 5
            string[] input = "0 2 0 8 1 0 7 4 0 7 0 0 0 0 3 1 0 0 0 9 0 0 0 2 8 0 5 0 0 9 0 4 0 0 8 7 4 0 0 2 0 8 0 0 3 1 6 0 0 3 0 2 0 0 3 0 2 7 0 0 0 6 0 0 0 5 6 0 0 0 0 8 0 7 6 0 5 1 0 9 0".Split();

            for (int c = 0; c < blockCoords.Length; c++)
            {
                int x = blockCoords[c].Item1;
                int y = blockCoords[c].Item2;

                for (int i = x; i < x + 3; i++)
                {
                    for (int j = y; j < y + 3; j++)
                    {
                        grid[i, j] = int.Parse(input[i * 9 + j]) - 1;

                        if (grid[i, j] > -1)
                        {
                            fixedCoords[i, j] = true;
                            blockFixedNumbers[c, grid[i, j]] = true;
                        }
                    }
                }
            }
        }

        // Fills the empty places in the Sudoku to create a valid problem state
        public static void FillGrid()
        {
            for (int c = 0; c < blockCoords.Length; c++)
            {
                int x = blockCoords[c].Item1;
                int y = blockCoords[c].Item2;

                int number = 0;

                for (int i = x; i < x + 3; i++)
                {
                    for (int j = y; j < y + 3; j++)
                    {
                        if (grid[i, j] == -1)
                        {
                            while (blockFixedNumbers[c, number])
                            {
                                number++;
                            }

                            grid[i, j] = number;
                            number++;
                        }
                    }
                }
            }
        }

    }
}
